var express = require('express');
var router = express.Router();
var config  =  require('../../../config/main')[process.env.NODE_ENV || 'development'];

var middleware = require('../../../middle.js');

var User = require('../../../models/user');

var profile = require('./profile');
var polls = require('./polls');
var classified = require('./classified');
var documents = require('./documents');
var notices = require('./notices');
var emergency = require('./emergency');
var search = require('./search');
var board = require('./board');
var contact = require('./contact');
var visitors = require('./visitors');
var employees = require('./employees');
var services = require('./services');
var bills = require('./bills');

var notifications = require('./notifications');


/** Login */

function requireLogin (req, res, next) {
  if (!req.session.authenticated || req.session.role != 'Resident') {
    res.redirect('/logout');
  } else {
    // "globally" set the username & user profile pic for the view rendered in next()
    res.locals.username = req.session.user.name;
    res.locals.profilePicUrl = req.session.user.profilePicUrl;
    next();
  }
};

router.post('/login', function (req, res){
	if (!req.body.contactEmail || !req.body.password) {
		return res.json({error: true, message: 'Please provide email and password'});
    } else {
      User.findOne({
        contactEmail: req.body.contactEmail,
        role: 'Resident'
      }, function (err, user) {
        if (err)
          throw err;
        if (!user) {
			return res.json({error: true, message: 'User not found'});
        } else {
          // don't allow deactivated (soft delete) residents to login
          if (user.softDeleted !== undefined && user.softDeleted === true) {
            return res.json({error: true, message: "Resident is Deactivated. Contact Admin."});
          }
          if (user.status !== undefined && user.status === "inactive") {
            return res.json({error: true, message: "Resident is Deactivated. Contact Admin."});
          }
          // don't allow residents of a inactive scoiety to login
          if (user.societyInactive !== undefined && user.societyInactive === true) {
            return res.json({error: true, message: "Your Society is Inactive!"});
          }
          // password checking
          user.comparePassword(req.body.password, function (err, isMatch) {
            if (isMatch && !err) {
				var data = {
                id: user._id,
                name: user.name,
                role: user.role,
                contactEmail: user.contactEmail,
                societyId: user.societyId,
                profilePicUrl: user.awsProfilePicURL,
                block: user.block, // added on 12/09/16 for My Documents
                flatNo: user.flatNo
              };
              console.log("Logging payload", data);
              req.session.authenticated = true;
              req.session.role = user.role;
              req.session.user = data;
              res.json({error: false});
            } else {
				return res.json({
					error: true,
					message: "Login Failed! Please Check Your Credentials."
				});
            }
          });
        }
      });
    }
});

router.get('/login', function(req, res, next) {
  res.render('resident/login', { title: 'Resident Login', error: false });
});


router.all('/logout', function (req, res){
  req.session.authenticated = false;
  req.session.user = null;
  req.session.role = null;
  res.redirect('/login');
});



router.get('/', requireLogin, middleware.getModulesSubscribed, middleware.awsGetTempCreds, profile.get);
router.post('/editProfile', requireLogin, profile.post);

router.get('/polls', requireLogin, middleware.getModulesSubscribed, polls.get);
router.post('/polls', requireLogin, polls.post);

router.get('/classified', requireLogin, middleware.getModulesSubscribed, classified.get);
router.get('/classified/add', requireLogin, middleware.getModulesSubscribed, middleware.awsGetTempCreds, classified.getNew);
router.get('/classified/edit/:adId', requireLogin, middleware.getModulesSubscribed, middleware.awsGetTempCreds, classified.getEdit);
router.get('/classified/myads', requireLogin, middleware.getModulesSubscribed, classified.getMyAds);
router.post('/classified', requireLogin, classified.postNew);
router.put('/classified/:adId', requireLogin, classified.put);
router.delete('/classified/:adId', requireLogin, classified.del);

router.get('/documents', requireLogin, middleware.getModulesSubscribed, middleware.awsGetTempCreds, documents.get);
router.post('/documents', requireLogin, documents.post);
router.delete('/documents', requireLogin, documents.del);

router.get('/notices', requireLogin, middleware.getModulesSubscribed, notices.get);

router.get('/emergency', requireLogin, middleware.getModulesSubscribed, emergency.get);

router.get('/residents', requireLogin, middleware.getModulesSubscribed, search.get);
router.post('/search', requireLogin, search.post);


router.get('/board', requireLogin, middleware.getModulesSubscribed, board.get);

router.get('/contacts', requireLogin, middleware.getModulesSubscribed, contact.get);
router.post('/contacts', requireLogin, contact.post);
router.post('/reply', requireLogin, contact.reply);


router.get('/visitors', requireLogin, middleware.getModulesSubscribed, visitors.get);
router.post('/visitors', requireLogin, visitors.post);
router.delete('/visitors', requireLogin, visitors.remove);


router.get('/employees', requireLogin, middleware.getModulesSubscribed, middleware.awsGetTempCreds, employees.get);
router.post('/employees', requireLogin, employees.addEmployee);
router.delete('/employees', requireLogin, employees.remove);


router.get('/services', requireLogin, middleware.getModulesSubscribed, services.get);
router.put('/addToMyService', requireLogin, services.addToMyService);
router.post('/services', requireLogin, services.post);

router.get('/bills', requireLogin, middleware.getModulesSubscribed, bills.get );


router.get('/terms', requireLogin, middleware.getModulesSubscribed, function (req, res){
  res.render('resident/terms');
});

router.get('/faq', requireLogin, middleware.getModulesSubscribed, function (req, res){
  res.render('resident/faq');
});

// Alerts (Notifications)
router.get("/alerts", requireLogin, middleware.getModulesSubscribed, notifications.get); // all alerts
router.post("/notifications", notifications.post); // Used by Ajax Polling
router.put("/notifications/:notifid", notifications.put); // mark read

// change own password
router.put('/changeownpass', function (req, res) {
  var newPass = req.body.newPass; // plaintext
  User
  .findOne({_id: req.session.user.id, role: "Resident" })
  .exec()
  .then(function (resident) {
    resident.password = newPass;
    return resident.save();
  })
  .then(function (savedResident) {
    return res.json({error: false, message: "Password Updated!"});
  })
  .catch(function (err) {
    return res.json({error: true, reason: err, message: "Failed to Update Password!"});
  })
})




module.exports = router;
