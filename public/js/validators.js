var validateEmail = function (email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

var validateAlphanumeric = function (txt) {
  var re = /^[a-z0-9]*$/i;
  return re.test(txt);
}

var validateNumeric = function (txt) {
  var re = /^[0-9]*$/i;
  return re.test(txt);
}

var validateLength = function (txt, len) {
  return (txt.length === len);
}

var validateRequired = function (txt) {
  return (txt !== undefined && txt !== null && txt.trim() !== "");
}
